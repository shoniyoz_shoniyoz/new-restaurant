module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        grey: "#ABABAF",
        gold: "#ECB45A",
        red: "#CF3734",
        black: "#100F0F",
      },
      lineHeight: {
        130: "130%",
        140: "140%",
      },
      zIndex: {
        1: "1",
        2: "2",
        3: "3",
        4: "4",
        5: "5",
      },
      fontFamily: {
        greycliff: ["Greycliff CF", "sans-serif"],
      },
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
