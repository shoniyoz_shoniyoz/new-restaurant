import { ref } from "vue";
import { useImagesStore } from "../stores/image";

const imageStore = useImagesStore();

export function useHandleFile(e: any) {
  const data = new FormData();
  const image = ref("");
  data.append("path", "restaurant");
  data.append("file", e.target.files[0]);
  imageStore.postImage(data).then((res: any) => {
    image.value = res?.data?.link;
  });
  return image;
}

export function useHandleFiles(e: any) {
  const data = new FormData();
  const images = ref("");
  const files = e.target.files;
  data.append("path", "restaurant");
  for (let i = 0; i < files.length; i++) {
    data.append("files", e.target.files[i]);
  }
  imageStore.postImages(data).then((res: any) => {
    images.value = res?.data?.links;
  });
  return images;
}
