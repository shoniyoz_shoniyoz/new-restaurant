import { defineStore } from "pinia";
import $axios from "@/plugins/axios";
import { removeItem, setItem } from "../helpers/persistaneStorage";
import { AxiosError, AxiosResponse } from "axios";
export const useAuth = defineStore("auth", {
  state: () => ({
    user: null,
  }),
  actions: {
    login(user: { username: string; password: string }) {
      return new Promise((resolve, reject) => {
        $axios
          .post("/auth/login", user)
          .then((respose: AxiosResponse) => {
            resolve(respose);
            removeItem("token");
            setItem("token", respose.data.token);
            $axios.defaults.headers.common.authorization = `Bearer ${respose.data.token}`;
          })
          .catch((error: AxiosError) => {
            reject(error);
          });
      });
    },
    logout() {
      removeItem("token");
    },
    fetchDelivery() {
      return new Promise((resolve, reject) => {
        $axios
          .get("/delivery")
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
});
