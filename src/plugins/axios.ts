import axios from "axios";
const $axios = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});
export default $axios;
$axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export const getToken: () => void = () => {
  const token: string | null = localStorage.getItem("token");

  if (token) {
    $axios.defaults.headers.common.authorization = `Bearer ${token}`;
  }
};
getToken();
