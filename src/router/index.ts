import { createRouter, createWebHistory } from "vue-router";

function authGuard(to: any, from: any, next: any) {
  if (localStorage.getItem("token")) {
    next();
  } else {
    next("/login");
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      meta: { layout: "default" },
      component: () => import("../pages/login/LIndex.vue"),
    },
    {
      path: "/",
      name: "home",
      meta: { layout: "main" },
      component: () => import("../pages/home/HIndex.vue"),
    },
    {
      path: "/delivery",
      name: "delivery",
      meta: { layout: "main" },
      component: () => import("../pages/delivery/DIndex.vue"),
    },
    {
      path: "/booking",
      name: "booking",
      meta: { layout: "main" },
      component: () => import("../pages/booking/BIndex.vue"),
    },
    {
      path: "/restaurant",
      name: "restaurant",
      meta: { layout: "main" },
      component: () => import("../pages/restaurant/RIndex.vue"),
    },
    {
      path: "/category-dishes",
      name: "categoryDishes",
      meta: { layout: "main" },
      component: () => import("../pages/categoryDishes/CDIndex.vue"),
    },
    {
      path: "/popular-dishes",
      name: "popularDishes",
      meta: { layout: "main" },
      component: () => import("../pages/popularDishes/PDindex.vue"),
    },
    {
      path: "/rooms",
      name: "rooms",
      meta: { layout: "main" },
      component: () => import("../pages/rooms/RIndex.vue"),
    },
    {
      path: "/staff",
      name: "staff",
      meta: { layout: "main" },
      component: () => import("../pages/staff/SIndex.vue"),
    },
  ],
});

export default router;
