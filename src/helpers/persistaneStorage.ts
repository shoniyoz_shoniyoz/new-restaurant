export const getItem = (key: string) => {
  try {
    return JSON.parse(localStorage.getItem(key) as string);
  } catch (error) {
    return null;
  }
};

export const setItem = (key: string, data: any) => {
  try {
    localStorage.setItem(key, data);
  } catch (error) {
    return null;
  }
};

export const removeItem = (key: string) => {
  try {
    localStorage.removeItem(key);
  } catch (error) {
    return null;
  }
};
