interface RowData {
  key: number;
  name: string;
  age: string;
  address: string;
}
export default RowData;
