interface RestaurantType {
  name: string;
  description: string;
  restaurant_type_id: number | null;
  allow_take_it: boolean;
  address: string;
  prepare_time_start: string;
  prepare_time_end: string;
  images: [];
  image: string;
  logo: string;
}

export default RestaurantType;
